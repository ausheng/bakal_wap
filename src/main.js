import Vue from 'vue';
import App from './App.vue'; 
import Vuex from 'vuex';
import router from './router';
import axios from 'axios';
import ElementUI from 'element-ui';
import './utils/directives';
Vue.use(ElementUI, {
    size: 'small'
});
Vue.use(Vuex);
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/css/icon.css';
import 'babel-polyfill';

import { post } from './utils/request';
import { localGet, localSet, localRemove, localClear } from './utils/storage';

Vue.prototype.$axios = axios;
Vue.prototype.$post = post;
Vue.prototype.$localGet = localGet;
Vue.prototype.$localSet = localSet;
Vue.prototype.$localClear = localClear;
Vue.prototype.$localRemove = localRemove;

const playerInfo = new Vuex.Store({
    state: {
        bakal_player_info: (!!localGet('bakal_player_info') && localGet('bakal_player_info') != 'undefined') ? localGet('bakal_player_info'): {},
        bakal_player_token: (!!localGet('bakal_player_info') && localGet('bakal_player_info') != 'undefined') ? localGet('bakal_player_token') : "",
        users_status: (!!localGet('bakal_player_info') && localGet('bakal_player_info') != 'undefined') ? true : false,
    },
    mutations: {
        changeUsersInfo(state, info) {
            console.log("changeUsersInfo", state, info);
            state.bakal_player_info = info;
        },
        changeUsersToken(state, token) {
            console.log("changeUsersToken", state, token);
            state.bakal_player_token = token;
        },
        changeStatus(state, value) {
            console.log("changeStatus", state, value);
            state.users_status = value;
        },
    }
});

new Vue({
    router,
    store:playerInfo,
    render: h => h(App)
}).$mount('#app');
