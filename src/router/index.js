import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard'
        },{
            path: '/',
            component: () => import('../components/Home.vue'),
            meta: { title: '卧底大作战 - 巴卡尔攻坚战小游戏' },
            children: [
                {
                    path: '/dashboard',
                    component: () => import('../components/page/Home/Dashboard.vue'),
                    meta: { title: '卧底大作战 - 巴卡尔攻坚战小游戏' }
                }, {
                    path: '/game-room',
                    component: () => import('../components/page/Home/GameRoom.vue'),
                    meta: { title: '卧底大作战 - 房间 ' }
                },
            ]
        }, {
            path: '/404',
            component: () => import('../components/page/404.vue'),
            meta: { title: 'Not Found' }
        }, {
            path: '*',
            redirect: '/404'
        }
    ]
});
