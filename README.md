#


> 巴卡尔攻坚战小游戏 - 谁是卧底 🚩

# <img src="http://blog.ivsheng.com/wp-content/uploads/2023/08/7.png" width="70%" height="100px">

<div>
	<a href="https://gitee.com/ausheng/bakal_wap" target="_blank">
        <img src="https://img.shields.io/badge/%E5%B7%B4%E5%8D%A1%E5%B0%94-%E5%8D%A7%E5%BA%95%E5%A4%A7%E4%BD%9C%E6%88%98-blue" />
    </a>
    <a href="https://gitee.com/ausheng/bakal_wap" target="_blank">
        <img src="https://img.shields.io/badge/%E5%B7%B4%E5%8D%A1%E5%B0%94%E6%94%BB%E5%9D%9A%E6%88%98-%E5%88%9B%E6%96%B0%E4%B8%96%E7%BA%AA-blue">
    </a>
    <a href="https://gitee.com/ausheng/bakal_wap" target="_blank">
        <img src="https://img.shields.io/badge/bakal_desktop-v1.0-brightgreen">
    </a>
    <a href="https://gitee.com/ausheng/bakal_wap" target="_blank">
        <img src="https://img.shields.io/badge/license-MIT-brightgreen" />
    </a>
</div>


## 开发记录

-   [x] 注册
-   [x] 登录/注销
-   [x] 游戏大厅
-   [x] 发布赛事
-   [x] 参加赛事
-   [x] 选择身份卡
-   [x] 查看游戏规则
-   [x] 投票/举报
-   [x] 最终结果
-   [x] 投票流水
-   [x] 即时通讯
-   [x] 聊天室
-   [x] 游戏风格页面


## 安装步骤

```
// 把模板下载到本地
git clone https://gitee.com/ausheng/bakal_wap.git      

// 进入模板目录
cd bakal_wap
        
// 安装项目依赖，等待安装完成之后，安装失败可用 cnpm 或 yarn
npm install         

// 开启服务器，浏览器访问 http://localhost:8080
npm run serve

// 执行构建命令，生成的dist文件夹放在服务器下即可访问
npm run build
```

## 👏 联系
# <img src="https://blog.ivsheng.com/wp-content/uploads/2023/08/ED06817F-1293-BD20-176F-D7AEF23FF641.jpg" />


## ⭐ Stars 


